from dlx import DLX


SUDOKU_SYMBOLS = "123456789"


class SudokuException(Exception):
    pass


class SudokuSolver(object):
    def __init__(self):
        columns = [(i, DLX.PRIMARY) for i in range(81*4)]
        rows = [[i // 9,
                (i // 81) * 9 + i % 9 + 81,
                i % 81 + 81*2,
                ((i // 243) * 3 + (i % 81) // 27) * 9 + i % 9 + 81 * 3]
                for i in range(729)]
        self.DLXInstance = DLX(columns)
        self.rowIndeces = self.DLXInstance.appendRows(rows)
        self.loadedRows = list()

    def load(self, sudokustring):
        while(len(self.loadedRows) > 0):
            self.DLXInstance.unuseRow(self.loadedRows.pop())
        if len(sudokustring) < 81:
            raise SudokuException
        for i in range(81):
            if sudokustring[i] in SUDOKU_SYMBOLS:
                number = int(sudokustring[i])
                index = i*9+number
                self.DLXInstance.useRow(self.rowIndeces[index-1])
                self.loadedRows.append(self.rowIndeces[index-1])

    def printSolutions(self):
        for solution in self.DLXInstance.solve():
            answer = [0 for i in range(81)]
            for rowIndex in solution:
                row = self.DLXInstance.getRowList(rowIndex)
                answer[min(row)] = max(row) % 9+1
            for i in range(81):
                print(answer[i], end=' ')
                if i % 9 == 8:
                    print()
            print("*" * 17)

if __name__ == "__main__":
    solver = SudokuSolver()
    solver.load("..............3.85..1.2.......5.7.....4...1...9.......5......73..2.1........4...9")
    solver.printSolutions()
    solver.load("..............3.85..1.2.......5.7.....4...1...9.......5......73..2.1........4...9")
    solver.printSolutions()
